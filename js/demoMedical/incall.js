var me = {};
me.avatar = "img/robot.jpg";

var you = {};
you.avatar = "img/icon-person.png";

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}            

function insertChat(who, text, time){
    if (time === undefined){
        time = 0;
    }
    var control = "";
    var date = formatAMPM(new Date());
    
    if (who == "me"){
        control = '<li style="width:80%">' +
                        '<div class="msj macro">' +
                        '<div class="avatar"><img class="img-circle" style="width:50%;" src="'+ me.avatar +'" /></div>' +
                            '<div class="text text-l">' +
                                '<p>'+ text +'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';                    
    }else{
        control = '<li style="width:80%;">' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+text+'</p>' +
                                '<p><small>'+date+'</small></p>' +
                            '</div>' +
                        '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:50%;" src="'+you.avatar+'" /></div>' +                                
                '</li>';
    }
    setTimeout(
        function(){
            $('#loaderDiv').hide()                   
            $("ul").append(control).scrollTop($("ul").prop('scrollHeight'));
        }, 
    time);
    
}

function resetChat(){
    $("ul").empty();
}

resetChat();

function cleanCode (sentance){
    let wordsLita = sentance.split(' ')
    let cleanList = [];
    wordsLita.forEach(element => {
        cleanList.push(lancaster(element))
    });
    return cleanList;
}

function bow(sentence, words){
    let bag = new Array(words.length).fill(0.0);
    sentence.forEach(s => {
        words.forEach((w, i)=>{
            if (w == s) {
                bag[i] = 1.0
            }
        })
    })
    return bag;
}

function chatButtonClick(btnobj){
    $(".mytext").val(btnobj.value)
    $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
}

$(document).ready(function() {
    var model;
    var _self = this;
    var data;
    var resp;

    (async function(){ 
        model = await tf.loadLayersModel("./js/demoMedical/model.json");
        data = await $.get("./js/demoMedical/data.json");
        resp = await $.get("./js/demoMedical/words.json");
    })();

    $(".mytext").on("keydown", function(e){
        if (e.which == 13){
            let tens;
            let bow_list;
            var text = $(this).val();
            findAnswer(text)
            $(this).val('');
        }
    });

    function findAnswer(text){
        if (text !== ""){  
            $('#loaderDiv').show();
            insertChat("you", text); //---------------------      
            (async function (){
                //cleanCode(text);
                bow_list = bow(cleanCode(text), resp.words);
                tens = tf.tensor(bow_list, [1, resp.words.length])
                prediction = await model.predict(tens);
                const values = prediction.dataSync();
                const arr = Array.from(values);
                let results = []
                let return_list = []
                let ERROR_THRESHOLD = 0.20
                arr.forEach((i,r)=>{
                    if(i > ERROR_THRESHOLD){
                        results.push([r, i])
                    }
                })
                results = results.reverse()
                results.forEach(i=>{
                    return_list.push([resp.classes[i[0]], i[1]])
                })
                let htmlString = "";
                data["articles"].forEach(intent => {
                    for(let x = 0; x < results.length; x++) {
                        if(intent["title"] == return_list[x][0]){
                            //htmlString += "<h1>" + intent["name"]+ "</h1>" + intent["body"]
                            htmlString += intent["body"]
                            if(intent["type"] == "button"){
                                htmlString += "<br/>";
                                intent["button"].buttonList.forEach(btn=>{
                                    htmlString += "<input type='button' class=chatbtnchoice onclick=chatButtonClick(this) value='" + btn.text + "'/> <br/>"
                                })  
                            }
                        }
                    }
                })
                if(htmlString == ""){
                    htmlString = "I am not sure about what you want, plz try again"
                }
                insertChat("me", htmlString, 500);//--------------------- 
            })();
        }
    
    }

    $('body > div > div > div:nth-child(2) > span').click(function(){
        $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
    });

     
    $('#welChat').hide();

    $('.openClose > a').on("click", function(){
        resetChat();
        $('.col-sm-4').hide()
        $('#welChat').show();
    });

    insertChat("me", "Hello, my name is bot, I am here to assist you on services", 400);

    $('#welChat').click(function(){
        $('#loaderDiv').show()  
        $('.col-sm-4').show()
        $('#welChat').hide();
        insertChat("me", "Hello, my name is bot, I am here to assist you on services", 400);
    })

});
